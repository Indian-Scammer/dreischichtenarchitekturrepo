package data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PromoCodeDataStub implements IPromoCodeData {
	private BufferedWriter writer;
	private Map<String, Boolean> savedPromoCodes;
	private File file;
	
	@Override
	public boolean savePromoCode(String code) {
		//Verhindert das Ersetzen einer PromoCode-Datei, falls schon vorhanden. Es h�ngt dann einfach eine Zahl hinter den Dateinamen.
		if (file == null) {
			file = new File("./PromoCodes.txt");
			for (int i = 1; file.exists(); i++)
				file = new File("./PromoCodes" + i + ".txt");
		}
		//PromoCode speichern
		try {
			//Wenn Writer null ist, ist auch die HashMap savedPromoCodes null
			if (writer == null) {
				writer = new BufferedWriter(new FileWriter(file));
				savedPromoCodes = new HashMap<String, Boolean>();
			}
			writer.write(code + "\n");
			writer.flush();
			savedPromoCodes.put(code, true);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isPromoCode(String code) {
		return (savedPromoCodes == null) ? false : (savedPromoCodes.get(code) != null);
	}

}
