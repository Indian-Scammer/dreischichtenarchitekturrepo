package start;

import gui.PromocodeGUI;
import gui.PromocodeTUI;
import logic.IPromoCodeLogic;
import logic.PromoCodeLogic;
import data.IPromoCodeData;
import data.PromoCodeDataStub;

public class PromoCodeStart {

	public static void main(String[] args) {
		IPromoCodeData data = new PromoCodeDataStub();
		IPromoCodeLogic logic = new PromoCodeLogic(data);

		new PromocodeTUI(logic);
		new PromocodeGUI(logic);
	
	}

}
