package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private String code;
	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setResizable(false);
		ImageIcon mainIcon = new ImageIcon ("./ressources/promo.png");
		Image newMainIcon = mainIcon.getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		mainIcon = new ImageIcon(newMainIcon);
		setIconImage(mainIcon.getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		code = logic.getNewPromoCode();
//		lblPromocode = new JLabel("");
//		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
//		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
//		contentPane.add(lblPromocode, BorderLayout.CENTER);
		
		JPanel pnlCenter = new JPanel();
		pnlCenter.setBackground(Color.WHITE);
		contentPane.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new BorderLayout(0, 0));
		
		
		JTextField txtPromocode = new JTextField(code);
		txtPromocode.setEditable(false);
		txtPromocode.setBackground(null);
		txtPromocode.setBorder(null);
		txtPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		txtPromocode.setFont(new Font("Courier New", Font.BOLD, 16));
		pnlCenter.add(txtPromocode, BorderLayout.CENTER);
		
		JButton btnRefresh = new JButton("");
		ImageIcon refreshIcon = new ImageIcon("./ressources/refresh.png");
		Image newRefreshIcon = refreshIcon.getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		refreshIcon = new ImageIcon(newRefreshIcon);
		btnRefresh.setIcon(refreshIcon);
		btnRefresh.setOpaque(false);
		btnRefresh.setContentAreaFilled(false);
		btnRefresh.setBorderPainted(false);
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				code = logic.getNewPromoCode();
				txtPromocode.setText(code);
				
			}
		});
		
		pnlCenter.add(btnRefresh, BorderLayout.EAST);
		
		JLabel lblBeschriftungPromocode = new JLabel("Ihr Promotioncode f�r doppelte IT$ / Stunde");
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblBeschriftungPromocode, BorderLayout.NORTH);
		this.setVisible(true);
		
		JButton btnCopyToClipboard = new JButton("");
		ImageIcon copyIcon = new ImageIcon("./ressources/copy.png");
		Image newCopyIcon = copyIcon.getImage().getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		copyIcon = new ImageIcon(newCopyIcon);
		btnCopyToClipboard.setIcon(copyIcon);
		btnCopyToClipboard.setOpaque(false);
		btnCopyToClipboard.setContentAreaFilled(false);
		btnCopyToClipboard.setBorderPainted(false);
		btnCopyToClipboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				StringSelection selection = new StringSelection(code);
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(selection, selection);
				
			}
		});
		
		contentPane.add(btnCopyToClipboard, BorderLayout.EAST);
		
	}

}
